const usersController = require('../controllers/users_controller');

module.exports = (app) => {
  // New user.
  app.post('/api/users/create', usersController.register);
};
