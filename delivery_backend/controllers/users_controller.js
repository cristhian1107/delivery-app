const User = require('../models/user');

// TODO: Main Object
module.exports = {
  /**
   * Register new user.
   * @param req - request.
   * @param res - response.
   */
  register (req, res) {
    // Capture data sent from the client
    const user = req.body;
    User.create(user, (err, data) => {
      if (err) {
        return res.status(501).json({
          success: false,
          message: 'Error al registrar usuario',
          error: err
        });
      } else {
        return res.status(201).json({
          success: true,
          message: 'El usuario se registro exitosamente',
          data: data // ID new user
        });
      }
    });
  }
};
