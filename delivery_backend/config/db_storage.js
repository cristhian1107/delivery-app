const mysql = require('mysql');

/**
 * Database configuration parameters.
 */
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'r00t',
  database: 'delivery_app'
});

/**
 * Init connect to Database.
 */
db.connect(function (err) {
  if (err) throw err;
  console.log('Database connected successful');
});

module.exports = db;
