const db = require('../config/db_storage');

// User type object.
const User = {
  email: undefined,
  name: undefined,
  lastname: undefined,
  phone: undefined,
  image: undefined,
  password: undefined,
  create_at: undefined,
  create_by: undefined,
  update_at: undefined,
  update_by: undefined
};

/**
 * Insert new user in config.
 * @param user - user object
 * @param result - result error or new id.
 */
User.create = (user, result) => {
  // Query SQL.
  const sql = `
    INSERT INTO users(
    email,
    name,
    lastname,
    phone,
    image,
    password,
    create_at,
    create_by,
    update_at,
    update_by)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;

  // Execute Query SQL
  db.query(
    sql,
    [
      user.email,
      user.name,
      user.lastname,
      user.phone,
      user.image,
      user.password,
      new Date(),
      user.name,
      new Date(),
      user.name
    ],

    /**
     * Create a new user and return his id.
     * @param err - Error.
     * @param res - Response.
     */
    (err, res) => {
      if (err) {
        console.log('Error!: ', err);
        result(err, null);
      } else {
        console.log('Id del nuevo usuario', res.insertId);
        result(null, res.insertId);
      }
    }
  );
};

module.exports = User;
