const express = require('express');
const http = require('http');
const logger = require('morgan');
const cors = require('cors');
/* ----Imports routes---- */
const usersRoutes = require('./routes/users_routes');

const app = express();
const server = http.createServer(app);
const port = process.env.PORT || 3000;

app.use(logger('dev')); // Debugger API
app.use(express.json()); // Json Format API
app.use(express.urlencoded({
  extended: true
}));
app.use(cors()); // Use CORS API
app.disable('x-powered-by'); // Security disable header X-Powered-By
app.set('port', port);

/**
 * Root path (GET)
 */
app.get('/', (req, res) => {
  res.send('Ruta raíz del backend');
});

// Call users routes.
usersRoutes(app);

/**
 * Init service API and configuration.
 */
server.listen(3000, '0.0.0.0' || 'localhost', function () {
  console.log('Aplicación de NodeJS ' + process.pid + ' Iniciando ...');
});

// TODO: ERROR HANDLER!
app.use(errorHandler);

function errorHandler (err, req, res, next) {
  console.error(err);
  res.status(err.status || 500).send(err.stack);
  next(err); // Not mandatory
}
