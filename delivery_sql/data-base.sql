-- =====================================
-- DataBase Configuration - Mysql 8.0.28
-- =====================================
CREATE DATABASE IF NOT EXISTS delivery_app;
USE delivery_app;

-- =====================================
-- Create tables - Cristhian Apaza
-- =====================================
CREATE TABLE IF NOT EXISTS users (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(250) NOT NULL	UNIQUE,
	name VARCHAR(100) NOT NULL,
	lastname VARCHAR(100) NOT NULL,
	phone VARCHAR(25) NOT NULL UNIQUE,
	image VARCHAR(250) NULL,
	password VARCHAR(90) NOT NULL,
	create_at TIMESTAMP(0) NOT NULL,
	create_by VARCHAR(100) NOT NULL,
	update_at TIMESTAMP(0) NOT NULL,
	update_by VARCHAR(100) NOT NULL
);

SELECT * FROM users;