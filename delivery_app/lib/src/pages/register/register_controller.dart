import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  /// Public method - Register new users.
  void register() {
    String email = emailController.text.trim();
    String name = nameController.text;
    String lastName = lastNameController.text;
    String phone = phoneController.text;
    String password = passwordController.text.trim();
    String confirmPassword = confirmPasswordController.text.trim();

    if (isValidForm(email, name, lastName, phone, password, confirmPassword)) {
      Get.snackbar('Bienvenido', 'Estas listo para usar al API');
    }
  }

  /// Public method - Check if info is correct.
  bool isValidForm(String email, String name, String lastName, String phone,
      String password, String confirmPassword) {
    if (email.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar tu email');
      return false;
    }
    if (!GetUtils.isEmail(email)) {
      Get.snackbar('Datos invalida', 'El email no es correcto');
      return false;
    }
    if (name.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar tu nombre');
      return false;
    }
    if (lastName.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar tu apellido');
      return false;
    }
    if (phone.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar tu tel�fono');
      return false;
    }
    if (password.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar tu password');
      return false;
    }
    if (confirmPassword.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar otra vez tu password');
      return false;
    }
    if (password != confirmPassword) {
      Get.snackbar('Datos invalida', 'Los passwords no coinciden');
      return false;
    }
    return true;
  }
}
