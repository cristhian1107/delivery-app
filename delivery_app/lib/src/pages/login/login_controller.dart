import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  /// Public method - Change page to register.
  void goToRegisterPage() {
    Get.toNamed('/register');
  }

  /// Public method - Check if the user exists.
  void login() {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();

    // print('Email $email');
    // print('Email $password');

    if (isValidForm(email, password)) {
      Get.snackbar('Bienvenido', 'Estas listo para usar al API');
    }
  }

  /// Public method - Check if [email] and [password] is correct.
  bool isValidForm(String email, String password) {
    if (email.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar un email');
      return false;
    }
    if (!GetUtils.isEmail(email)) {
      Get.snackbar('Datos invalida', 'El email no es correcto');
      return false;
    }
    if (password.isEmpty) {
      Get.snackbar('Datos invalida', 'Debe ingresar un password');
      return false;
    }
    return true;
  }
}
